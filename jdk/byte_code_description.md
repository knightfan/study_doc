##部分字节码结构说明
1. tableswitch
tableswitch指令结构如下：
| tableswitch指令结构|
|-------------------|
|   tableswitch     |
|	0~3字节填充      |
|	default4byte	|
|	low4byte	    |
|	high4byte	    |
|	jumpoffset4byte |
|	.....           |

开始是tableswitch=0xaa表示tableswitch指令开始
接着填充0~3字节，使得default4byte首地址与方法入口地址(通常为0)为4的倍数
default4byte 跳转地址
low4byte switch最小值
high4byte switch最大值
high4byte-low4byte+1 个jumpoffset4byte，表示有high4byte-low4byte+1跳转地址

执行步骤： 1.取栈顶操作数index，如果index不在[low4byte,high4byte],则执行default，否则查找第 index-low 个跳转地址，找到这个地址后再跳转到实际指令执行。
if(index lt low4byte || index gt high4byte) {
goto default
}else{
goto (index-low)
}

举个栗子：switch代码
````java
public class TestSwitch {
	public void test(int i) {
		switch (i) {
		case 0:
			i = 1;
			break;
		case 2:
			i = 3;
			break;
		default:
			break;
		}
	}
}

编译之后用javap -v TestSwitch 截取该方法字节码
         0: iload_1			
         1: tableswitch   { // 0 to 2
                       0: 28
                       1: 38
                       2: 33
                 default: 38
            }
        28: iconst_1
        29: istore_1
        30: goto          38
        33: iconst_3
        34: istore_1
        35: goto          38
        38: return
````


  7a 00 01 00 02 00 00 00  27 **1b aa 00 00 00 00 00  
  25 00 00 00 00 00 00 00  02 00 00 00 1b 00 00 00  
  25 00 00 00 20 04 3c a7  00 08 06 3c a7 00 03 b1**

1b为将第一个局部变量压到操作数栈
aa 为tableswitch操作码索引，表示这是一条tableswitch指令，以下跳转地址+0x01原因是这条指令aa所在的索引为1，第0索引为1b。
00 00 为两个填充字节，使得default首字节为4，即为4的倍数
00 00 00 25 为default跳转地址=0x25+0x01=38
00 00 00 00 为case最低值0
00 00 00 02 为case最大值2
紧接着是2-0+1=3个jumpoffset，分别表示当case=0,1,2时跳转地址
00 00 00 1b 表示当case=0时跳转地址0xb+0x01=28
00 00 00 25 表示当case=1时跳转地址0x25+0x01=38
00 00 00 20 表示当case=2时跳转地址0x20+0x01=33

2.lookupswitch
lookupswitch 指令结构如下：

|lookupswitch指令结构|
|-------------------|
|   lookupswitch    |
|	0~3字节填充      |
|	default4byte	|
|	npaire4byte	    |
|	match-offset    |
|	.....           |


开始是lookupswitch=0xab表示lookupswitch指令开始
接着填充0~3字节，使得default4byte首地址与方法入口地址(通常为0)为4的倍数
default4byte 4字节跳转地址
npaire4byte 4字节表示键值对个数也就是case数量
match-offset 4字节表示值，4字节跳转地址

举个栗子：switch代码
````java
public class TestSwitch{
	public void test(int i){
		switch (i) {
		case 0:
			i = 1;
			break;
		case 102:
			i = 3;
			break;
		default:
			break;
		}
	}
}


编译之后用javap -v TestSwitch 截取该方法字节码
         0: iload_1
         1: lookupswitch  { // 2
                       0: 28
                     102: 33
                 default: 38
            }
        28: iconst_1
        29: istore_1
        30: goto          38
        33: iconst_3
        34: istore_1
        35: goto          38
        38: return

````

  00 00 00 5e 00 01 00 02  00 00 00 27 **1b ab 00 00  
  00 00 00 25 00 00 00 02  00 00 00 00 00 00 00 1b  
  00 00 00 66 00 00 00 20  04 3c a7 00 08 06 3c a7  
  00 03 b1** 00 00 00 02 00  07 00 00 00 1a 00 06 00  

1b为将第一个局部变量压到操作数栈
ab 为lookupswitch操作码索引，表示这是一条lookupswitch指令，以下跳转地址+0x01原因是这条指令ab所在的索引为1，第0索引为1b。
00 00 为两个填充字节，使得default首字节为4，即为4的倍数
00 00 00 25 为default跳转地址=0x25+0x01=38
00 00 00 02 为键值对个数，也就是case个数，2个case条件
紧接着是2个match-offset，分别为4字节match，4字节跳转
00 00 00 00 00 00 00 1b 当值匹配上0时，跳转到地址0x1b+0x01=28
00 00 00 66 00 00 00 20 当值为0x66=102时，跳转地址0x20+0x01=33

lookupswitch 尽可能是有序的case条件，以便能使用比线性查找更快的办法。
> The match-offset pairs are sorted to support lookup routines thatare quicker than linear search.