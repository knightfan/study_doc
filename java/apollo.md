##Apollo配置中心
配置特性:
1. 独立于程序的只读变量,同一个程在不同的配置下有不同的行为,程序通过配置变量改变自身的行为.
2. 配置伴随应用的整个生命周期.
3. 多种方式加载配置,如hard code,配置文件,启动参数,数据库配置等等.
4. 配置治理,权限控制;环境(开发,测试,生产)、集群配置(不同的数据中心)管理;框架类组件配置管理 
Apollo解决的问题:
1. 统一管理不同环境(environment)、不同集群(cluster),不同命名空间(namespace)的配置,通过命名空间方便不同的应用共享一份配置.
2. 配置修改实时生效（热发布）,配置发布后可以通知应用程序变更.
3. 版本发布管理,配置发布后生成版本快照,以便回滚.
4. 灰度发布,配置变更后支持部分应用受到配置更新通知,待验证没问题就在通知到所有的应用.
5. 权限管理、发布审核、操作审计
6. 客户端配置信息监控,界面方便管理配置
7. 提供Java和.Net原生客户端
8. 提供开放平台API
9. 部署简单,jar包,依赖jdk与mysql即可.

###Apollo整理架构
![Apollo架构图](pic/apollo-overall-architecture.png)

1. Config Service提供配置的读取、推送等功能，服务对象是Apollo客户端
2. Admin Service提供配置的修改、发布等功能，服务对象是Apollo Portal（管理界面）
3. Config Service和Admin Service都是多实例、无状态部署，所以需要将自己注册到Eureka中并保持心跳
4. 在Eureka之上我们架了一层Meta Server用于封装Eureka的服务发现接口
5. Client通过域名访问Meta Server获取Config Service服务列表（IP+Port），而后直接通过IP+Port访问服务，同时在Client侧会做load balance、错误重试
6. Portal通过域名访问Meta Server获取Admin Service服务列表（IP+Port），而后直接通过IP+Port访问服务，同时在Portal侧会做load balance、错误重试
7. 为了简化部署，我们实际上会把Config Service、Eureka和Meta Server三个逻辑角色部署在同一个JVM进程中

###eureka特点:
1. 提供了完整的Service Registry和Service Discovery实现 
2. 和Spring Cloud无缝集成, 集成到应用中,减少外部依赖(像zk等),提高配置中心的可用性和降低部署复杂度

###各个模块
1. Config Service
配置服务,保存配置信息;
提供查询配置接口;推送配置更新（基于Http long polling,Spring DeferredResult异步化）;
接口服务对象为Apollo客户端.
2. Admin Service
提供配置管理接口
提供配置修改、发布等接口
接口服务对象为Portal
3. Meta Server
Portal,Client通过Meta Server获取AdminService与ConfigService的ip与端口
Meta Server从eureka获取AdminService与ConfigService的ip与端口
Meta Server的角色主要是为了封装服务发现的细节, 对外提供Http接口获取AdminService与ConfigService的ip与端口,客户端只需通过Http即可获取到对应的服务地址,不需关系后面的服务注册与发现组件.
与ConfigService于同一个JVM进程
4. Eureka
服务的注册与发现,与ConfigService于同一个JVM进程
Config Service和Admin Service会向Eureka注册服务，并保持心跳
5. Portal
提供Web界面供用户管理配置
通过Meta Server获取Admin Service服务列表（IP+Port），通过IP+Port访问服务
在Portal侧做load balance、错误重试
6. Client
Apollo提供的客户端程序，为应用提供配置获取、实时更新等功能
通过Meta Server获取Config Service服务列表（IP+Port），通过IP+Port访问服务
在Client侧做load balance、错误重试

###主体E-R Diagram
App:App信息
AppNamespace:App下Namespace的元信息
Cluster:集群信息
Namespace:集群下的namespace
Item:Namespace的配置，每个Item是一个key, value组合
Release:Namespace发布的配置，每个发布包含发布时该Namespace的所有配置
Commit:Namespace下的配置更改记录
Audit:审计信息，记录用户在何时使用何种方式操作了哪个实体。

###服务端运行
1. 通知客户端方式:
用户在Portal操作配置发布
Portal调用Admin Service的接口操作发布
Admin Service发布配置后，发送ReleaseMessage给各个Config Service
Config Service收到ReleaseMessage后，通知对应的客户端

2. 发送ReleaseMessage的实现方式,也就是AdminService通知ConfigService方式


- AdminService发布配置后,需通知所有的ConfigService,ConfigService再通知所有的客户端更新最新配置,这是一个消息场景,可以使用消息组件来解耦AdminService与ConfigService
- Apollo的使用场景(配置),减少外部依赖,没有引入消息组件,通过数据库方式来实现.
- AdminService发布配置后再ConfigServiceDB.ReleaseMessage插入一条记录,消息内容就是配置发布的AppId+Cluster+Namespace，参见DatabaseMessageSender.
- ConfigService有一个线程每隔一秒扫描ConfigServiceDB.ReleaseMessage,如有发现新的记录(超过本地保存的变量maxIdScanned)会通知到所有的消息监听器（ReleaseMessageListener），如NotificationControllerV2，消息监听器的注册过程参见ConfigServiceAutoConfiguration
- NotificationControllerV2得到配置发布的AppId+Cluster+Namespace后，会通知对应的客户端

ConfigServiceAutoConfiguration.releaseMessageScanner
```java
	//ReleaseMessageScanner注册扫描监听器.
    @Bean
    public ReleaseMessageScanner releaseMessageScanner() {
      ReleaseMessageScanner releaseMessageScanner = new ReleaseMessageScanner();
      //0. handle release message cache
      releaseMessageScanner.addMessageListener(releaseMessageServiceWithCache);
      //1. handle gray release rule
      releaseMessageScanner.addMessageListener(grayReleaseRulesHolder);
      //2. handle server cache
      releaseMessageScanner.addMessageListener(configService);
      releaseMessageScanner.addMessageListener(configFileController);
      //3. notify clients
      releaseMessageScanner.addMessageListener(notificationControllerV2);
      releaseMessageScanner.addMessageListener(notificationController);
      return releaseMessageScanner;
    }
```

###客户端运行
- 客户端和服务端保持了一个长连接，从而能第一时间获得配置更新的推送。（通过Http Long Polling实现）
- 客户端还会定时从Apollo配置中心服务端拉取应用的最新配置。 
fallback机制,防止服务端没有推送或推送失败 导致配置没有更新.
客户端定时上报本地版本,如配置没有变更,服务端返回304 - Not Modified
客户端定时拉取的时间间隔为5min,可以通过配置变量apollo.refreshInterval来指定时间间隔(单位:分)
- 客户端从Apollo配置中心服务端获取到应用的最新配置后，会保存在内存中
- 客户端会把从服务端获取到的配置在本地文件系统缓存一份,在网络不可用或服务(ConfigService)不可用时,能从本地获取到配置.
- 应用程序可以从Apollo客户端获取最新的配置、订阅配置更新通知


###SpringBoot集成

SpringBoot启动时候会创建一个环境ConfigurableEnvironment,默认实现是StandardEnvironment,Web环境的话是StandardServletEnvironment,其父类AbstractEnvironment中的成员变量

```java
public abstract class AbstractEnvironment implements ConfigurableEnvironment {
	//..省略
	// 保存了提供获取配置的源,PropertySource
	private final MutablePropertySources propertySources = new MutablePropertySources();

	private final ConfigurablePropertyResolver propertyResolver =
			new PropertySourcesPropertyResolver(this.propertySources);
	//..省略
	// 获取配置,实际上调用PropertySourcesPropertyResolver.getProperty方法
	public String getProperty(String key) {
		return this.propertyResolver.getProperty(key);
	}
	
}

public class MutablePropertySources implements PropertySources {
	//配置源列表
	private final List<PropertySource<?>> propertySourceList = new CopyOnWriteArrayList<>();
	//添加到第一个位置
	public void addFirst(PropertySource<?> propertySource) {
		removeIfPresent(propertySource);
		this.propertySourceList.add(0, propertySource);
	}
	//添加到最后一个位置
	public void addLast(PropertySource<?> propertySource) {
		removeIfPresent(propertySource);
		this.propertySourceList.add(propertySource);
	}
	//..省略
}

// PropertySourcesPropertyResolver.getProperty
// 按顺序遍历所有的配置源, 如果发现满足条件的配置,则返回.
protected <T> T getProperty(String key, Class<T> targetValueType, boolean resolveNestedPlaceholders) {
		if (this.propertySources != null) {
			for (PropertySource<?> propertySource : this.propertySources) {
				if (logger.isTraceEnabled()) {
					logger.trace("Searching for key '" + key + "' in PropertySource '" +
							propertySource.getName() + "'");
				}
				Object value = propertySource.getProperty(key);
				if (value != null) {
					if (resolveNestedPlaceholders && value instanceof String) {
						value = resolveNestedPlaceholders((String) value);
					}
					logKeyFound(key, propertySource, value);
					return convertValueIfNecessary(value, targetValueType);
				}
			}
		}
		if (logger.isTraceEnabled()) {
			logger.trace("Could not find key '" + key + "' in any property source");
		}
		return null;
	}

```

MutablePropertySources保存了所有可以提供配置的源(PropertySource),
PropertySourcesPropertyResolver保存了MutablePropertySources的引用,
在获取配置时PropertySourcesPropertyResolver,顺序遍历MutablePropertySources的源,
如果能找到对应的值,则返回, 因此顺序还是比较重要的.

如果要优先获取Apollo的配置,可以在启动时候添加到MutablePropertySources的首位,这样获取配置的时候就能优先获取到.

###实时更新实现
实时更新,ApolloProcessor,实现了BeanPostProcessor,在bean初始化之前处理调用postProcessBeforeInitialization实现处理.
Spring的 @Value 注解,对应SpringValueProcessor处理
@ApolloConfigChangeListener 注解,对应 ApolloAnnotationProcessor
@ApolloJsonValue 注解,对应ApolloJsonValueProcessor

