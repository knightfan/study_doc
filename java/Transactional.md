##spring boot事务处理机制
在 https://start.spring.io/ 创建一个spring boot项目,这里填上group id为:com.example,artificial id为:test-transactional,然后下载下来导入IDE


```java
@SpringBootApplication
@RestController
public class App {

	public App() {
		System.out.println("App init");
	}

	@Resource
	private UserMapper userMapper;

	@RequestMapping("/")
	@Transactional
	public String rest(Integer id) {
		System.out.println("param is " + id);
		User user = userMapper.select(id);
		String uname = user.getFUsername();
		user.setFUsername(id + uname);
		user.setFPassword("password" + id);
		System.out.println("insert row affect :" + userMapper.save(user));
		System.out.println("update row affect :" + userMapper.update(uname, id));
		return "Hello " + uname;
	}

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}

```
