###缓存热点key问题
参考链接: 
[https://yq.aliyun.com/download/2456](https://yq.aliyun.com/download/2456 "热点Key的发现与解决之道")
热点key问题:
1. 用户消费的数据远大于生产的数据
2. 热卖商品,评论等
3. 请求分片集中,达到单台server极限.

热点key产生的问题:
1. 流量集中,达到物理网卡上限
2. 请求过多,分片服务被打垮
3. 缓存击穿,请求穿透,引起雪崩

常用解决方案:

服务端缓存
- 基于本地cache LRU策略的缓存
- 缓存失效/丢失,多线程更新缓存问题(读写锁同步)
- 数据不一致,脏读问题

使用redis,memcache
- 缓存独立部署
- 基于memcache/redis淘汰策略
- 数据不一致,脏读问题
 
读写分离
![读写分离架构](pic/hot_key.png)

SLB 层做负载均衡
Proxy 层做读写分离自动路由
Master 负责写请求
ReadOnly 节点负责读请求
Slave 节点和 Master 节点做高可用
实际过程中 Client 将请求传到 SLB，SLB 又将其分发至多个 Proxy 内，通过 Proxy 对请求的识别，将其进行分类发送。
例如，将同为 Write 的请求发送到 Master 模块内，而将 Read 的请求发送至 ReadOnly 模块。
而模块中的只读节点可以进一步扩充，从而有效解决热点读的问题。
读写分离同时具有可以灵活扩容读热点能力、可以存储大量热点Key、对客户端友好等优点。

热点数据发现
- 基于统计阈值的热点统计
- 基于统计周期的热点统计
- 基于版本号实现的无需重置初值统计方法
- DB 计算同时具有对性能影响极其微小、内存占用极其微小等优点

###布隆过滤器

###cookie，session，token
由于http是无状态的，为了是网站能共享数据，session和cookie出现了，服务器接收到客户端请求时，通常会添加sessionID以便区分客户端，tomcat通常会添加key=JSESSIONID的cookie 两者区别如下：
1. cookie是保存在客户的浏览器，session数据是存放在服务器
2. cookie不是很安全，用户可以盗取本地cookie进行欺骗，安全性要求高的应当使用session
3. session在一定时间内保存到服务器，当访问量增加时，会影响服务器性能(内存增加等)。
4. 单个cookie大小存储限制，浏览器都限制一个站点最多保存cookie的个数。
token与cookie类似，也是访问令牌，不同于cookie由浏览器自动添加，需开发人员自己维护。

CSRF攻击：CSRF跨站点请求伪造(Cross—Site Request Forgery)
攻击过程：假设存在CSRF漏洞的网站A，攻击者网站B，网站A的用户者C。
1. 用户C输入用户名密码登录A，A在用户C的浏览器设置了cookie，用户C可以正常访问A。
2. 用户C未退出，在同一浏览器打开网站B，B返回用户一段代码，这段代码是要请求A。
3. 根据B返回的代码，用户并不知道访问B的时候会携带A的cookie向A发送请求，具体请求内容完全由B返回的代码控制，网站A也并不知道这个请求时由B发起的，会根据用户C的cookie执行C的请求，导致B的代码被执行。
防范：1.验证 HTTP Referer 字段，浏览器在请求时会带上Referer字段，表明此请求的来源，如果不是同一Website的可以拒绝，但可能由于浏览器漏洞导致被篡改，或被用户禁止Referer
2.关键请求手动添加token，get请求可以放于url参数，post请求可以建立hidden字段，由于攻击者不知道token，也不存放在cookie中，攻击者B的代码无法插入token，网站A可以拦截B发出的请求。


###memcache,redis
1. 都是KV内存数据库，memcache可以缓存其他东西如图片等
2. redis支持多种数据结构，如list，set，sortset，hash，geo等等。
3. redis在内存快满时可以缓存到交换空间(磁盘)去。
4. 过期策略，memcache在set的时候指定，redis可以使用命令expire指定。
5. 都支持分布式，memcache可以使用magent指定一主多从，redis可以一主多从，多主多从(一致性hash算法将key分布到多台主服务器)等。
6. 数据安全，灾备，memcache在down机或关闭后，数据不可恢复；redis可以定期持久化到磁盘中RDB快照(可恢复)，或者AOF日志恢复。
7. redis支持备份，master-slave模式备份。
8. redis在数据操作上只能用于单核，memcache可以支持多核，，所以平均每一个核上Redis在存储小数据时比Memcached性能更高。而在100k以上的数据中，Memcached性能要高于Redis。
9. Memcached单个key-value大小有限，一个value最大只支持1MB，而Redis最大支持512MB 


###rpc序列化
Kryo:JAVA 定制的序列化协议,序列化后字节数少，利于网络传输。但不支持跨语言或要改造支持,不支持增删字段,非线程安全.
Hessian:支持跨语言，序列化后字节数适中
Protostuff:与语言无关,需要使用特定的语法编写.prpto文件,然后静态编译,性能略优于Hessian
json:文本协议,体积很大,跨语言支持


###生产者数据超过消费者消费能力
1. 数据是否可丢弃,可丢弃生产者丢弃
2. 实时性要求不高,消息中间件缓存(持久化存储),消费者再定时拉取
3. 实时性要求高,增加消费者消费能力(增加硬件/优化消费者性能)



