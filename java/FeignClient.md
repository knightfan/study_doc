在spring boot中,很多可以使用EnableXX注解来启用某些特定功能,如@EnableFeignClients,@EnableAsync,@EnableDiscoveryClient等等

这些特性如何被处理的，写个简单的FeignClient客户端试试。

```java
//FeignClientApp
//启用服务自动发现,貌似在2.1.6版本已经没什么用了，即使不用这个注解，如果相关classpath存在服务注册相关的类(consul,eureka等等)，也能注册与发现服务
//这个注解也没import什么实质的操作，只是引入了AutoServiceRegistrationConfiguration
@EnableDiscoveryClient	
@SpringBootApplication
@EnableFeignClients		//启用FeignClient客户端
public class FeignClientApp {

	//默认是根据type注入，可以指定名称(contextId或value或neme+FeignClient)
	@Resource
	private FeignTest feignTest;

	@PostConstruct
	public void execute() {
		System.out.println(feignTest.get());
	}

	public static void main(String[] args) {
		SpringApplication.run(FeignClientApp.class, args);
	}
}


@FeignClient(value = "${test.service.provider}")
public interface FeignTest {

	@PostMapping("/get")
	public String get();

}

```

@EnableFeignClients注解定义,这个注解被Import注解定义,Import了一个FeignClientsRegistrar类
```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(FeignClientsRegistrar.class)
public @interface EnableFeignClients {
	//扫描路径,包名称,跟basePackages一样
	String[] value() default {};
	String[] basePackages() default {};
    //指定类所在的包，例如java.lang.String，则扫描的包为java.lang。
	Class<?>[] basePackageClasses() default {};
	//默认配置类，加了@Configuration注解的类
	Class<?>[] defaultConfiguration() default {};
	//加了@FeignClient注解的类，如果不为空，则不扫描类路径了
	Class<?>[] clients() default {};
}
```

@FeignClient注解，只能注解在接口上
```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FeignClient {
	//调用服务的名称，可以不包含协议，跟name是一样的意思，可以使用变量占位符如${propertyKey}，所有的client必须要有的名字
	@AliasFor("name")
	String value() default "";
	@AliasFor("value")
	String name() default "";
	//跟name或value一样
	@Deprecated
	String serviceId() default "";
	//FeignClientSpecification,bean的名称，不作为调用服务的名称，如果没配置的话，将会使用name或value作为bean名称
	String contextId() default "";
	//跟@Qualifier一样,如果没有配置的话，默认使用contextId+"FeignClient"作为FeignClientFactoryBean的bean名称
	String qualifier() default "";
	//url，绝对路径或域名，协议可以不指定
	String url() default "";
	//是否返回404，避免抛FeignExceptions
	boolean decode404() default false;
	//自定义配置@Configuration注解类
	Class<?>[] configuration() default {};
	//降级类，必须实现了当前注解的接口，且是spring管理的bean	
	Class<?> fallback() default void.class;
	//降级工厂类，要能够创建出一个实例，这个实例实现了当前@FeignClient注解的接口，这个工厂类必须是spring管理的bean	
	Class<?> fallbackFactory() default void.class;
	//服务访问的路径，这个路径作用于全部的方法(在全部方法路径前缀)
	String path() default "";
	//默认是主代理类
	boolean primary() default true;
}
```

spring在refresh的invokeBeanFactoryPostProcessors是会调用配置处理的ConfigurationClassPostProcessor.postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry),这个处理类扫描到@Import注解类，如果import的是ImportBeanDefinitionRegistrar的实现类，在处理时会调用方法registerBeanDefinitions(AnnotationMetadata metadata,BeanDefinitionRegistry registry)方法


###FeignClientsRegistrar.java
FeignClientsRegistrar的registerBeanDefinitions方法
1. 注册一个默认配置类bean，使用@EnableFeignClients的配置属性defaultConfiguration，注册一个FeignClientSpecification的BeanDefinition
2. 扫描FeignClient注解，为每个接口注册一个bean配置FeignClientSpecification，并为每个接口注册一个FeignClientFactoryBean的BeanDefinition，加入到容器中，bean名称为(contextId或name或value)+FeignClient，FeignClientFactoryBean会保存FeignClient的属性值，使用BeanDefinitionBuilder配置到对应的FeignClientFactoryBean中去

```java

	public void registerBeanDefinitions(AnnotationMetadata metadata,
			BeanDefinitionRegistry registry) {
		registerDefaultConfiguration(metadata, registry);
		registerFeignClients(metadata, registry);
	}

	//EnableFeignClients里面属性处理
	private void registerDefaultConfiguration(AnnotationMetadata metadata,
			BeanDefinitionRegistry registry) {
		//获取@EnableFeignClients注解的全部属性
		Map<String, Object> defaultAttrs = metadata
				.getAnnotationAttributes(EnableFeignClients.class.getName(), true);

		//@EnableFeignClients有defaultConfiguration成员注册一个BeanClass是FeignClientSpecification(两个成员配置名与配置类)的BeanDefinition
		if (defaultAttrs != null && defaultAttrs.containsKey("defaultConfiguration")) {
			String name;
			if (metadata.hasEnclosingClass()) {
				name = "default." + metadata.getEnclosingClassName();
			}
			else {
				name = "default." + metadata.getClassName();
			}
			//注册一个BeanClass是FeignClientSpecification的BeanDefinition
			registerClientConfiguration(registry, name,
					defaultAttrs.get("defaultConfiguration"));
		}
	}
	
	//注册一个BeanClass是FeignClientSpecification的BeanDefinition(默认为GenericBeanDefinition)，将BeanDefinition注册到BeanDefinitionRegistry(默认实现为DefaultListableBeanFactory)
	private void registerClientConfiguration(BeanDefinitionRegistry registry, Object name,Object configuration) {
		BeanDefinitionBuilder builder = BeanDefinitionBuilder
				.genericBeanDefinition(FeignClientSpecification.class);
		//构造函数FeignClientSpecification(String name, Class<?>[] configuration) 
		builder.addConstructorArgValue(name);
		builder.addConstructorArgValue(configuration);
		//注册一个BeanDefinition
		registry.registerBeanDefinition(
				name + "." + FeignClientSpecification.class.getSimpleName(),
				builder.getBeanDefinition());
	}

	public void registerFeignClients(AnnotationMetadata metadata,
			BeanDefinitionRegistry registry) {
		ClassPathScanningCandidateComponentProvider scanner = getScanner();
		scanner.setResourceLoader(this.resourceLoader);

		Set<String> basePackages;

		//...获取类扫描FeignClient的路径，如果EnableFeignClients有配置clients属性，则取clients配置
		//没有的话取value，basePackages，basePackageClasses，全都为空的话，获取注解了EnableFeignClients的类所在的包，一般会在启动类注解EnableFeignClients并放在项目外层

		for (String basePackage : basePackages) {
			Set<BeanDefinition> candidateComponents = scanner
					.findCandidateComponents(basePackage);
			for (BeanDefinition candidateComponent : candidateComponents) {
				if (candidateComponent instanceof AnnotatedBeanDefinition) {
					//获取FeignClient中配置的contextId或value或name名称，
					String name = getClientName(attributes);
					//注册一个bean名称为name+FeignClientSpecification的配置bean
					registerClientConfiguration(registry, name,
							attributes.get("configuration"));
					//注册一个FeignClientFactoryBean，bean名称为(contextId或name或value)+FeignClient，FeignClient的属性会保存的FeignClientFactoryBean中，设置根据类型注入，默认为优先选择
					registerFeignClient(registry, annotationMetadata, attributes);
				}
			}
		}
	}
	
	//注册一个BeanDefinition，bean名字为接口的类名，别名为(contextId或name或value)+FeignClient或自定义(FeignClient.qualifier)
	private void registerFeignClient(BeanDefinitionRegistry registry,
			AnnotationMetadata annotationMetadata, Map<String, Object> attributes) {
		//className是注解所在的接口类名(包括包名)
		String className = annotationMetadata.getClassName();
		BeanDefinitionBuilder definition = BeanDefinitionBuilder
				.genericBeanDefinition(FeignClientFactoryBean.class);
		//...设置属性
		definition.addPropertyValue("url", getUrl(attributes));
	
		//别名
		String qualifier = getQualifier(attributes);
		if (StringUtils.hasText(qualifier)) {
			alias = qualifier;
		}
		//构造函数第二个为beanName，BeanDefinitionHolder(BeanDefinition beanDefinition, String beanName, @Nullable String[] aliases) 
		BeanDefinitionHolder holder = new BeanDefinitionHolder(beanDefinition, className,
				new String[] { alias });
		BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry);
	}
```

###FeignClientFactoryBean定义与数据结构，字段基本上与FeignClient注解一致
```java
class FeignClientFactoryBean
		implements FactoryBean<Object>, InitializingBean, ApplicationContextAware {
private Class<?> type;

	private String name;

	private String url;

	private String contextId;

	private String path;

	private boolean decode404;

	private ApplicationContext applicationContext;

	private Class<?> fallback = void.class;

	private Class<?> fallbackFactory = void.class;
}
```

###FeignClient生成代理类与注入

在DefaultListableBeanFactory的父类AbstractAutowireCapableBeanFactory执行doCreateBean时，也就是开头例子中的实例化FeignClientApp，发现需要注入FeignTest类型的bean，会递归查找FeignTest的bean，由于前面例子的Resource注解没有指定名字，会把当前的类型名称FeignTest(包括包名)当成是bean的名称
```java
org.springframework.beans.factory.support.AbstractBeanFactory.doGetBean(String, Class<T>, Object[], boolean){
	//从singletonObjects(Map)获取到的sharedInstance是刚刚注册的FeignClientFactoryBean
	Object sharedInstance = getSingleton(beanName);
	//最终会调用FactoryBean的getObject方法
	bean = getObjectForBeanInstance(sharedInstance, name, beanName, null);
}

//FeignClientFactoryBean.getObject()方法返回实际的bean，这个bean
是实际代理对象，通过动态代理生成的类
	public Object getObject() throws Exception {
		return getTarget();
	}
	//getTarget
	<T> T getTarget() {
		FeignContext context = this.applicationContext.getBean(FeignContext.class);
		//默认是feign.Feign.Builder，启用feign.hystrix.enabled=true，实例变为feign.hystrix.HystrixFeign.Builder
		Feign.Builder builder = feign(context);
		//....
		//默认target为DefaultTargeter,hystrix为HystrixTargeter
		Targeter targeter = get(context, Targeter.class);
		return (T) targeter.target(this, builder, context,
				new HardCodedTarget<>(this.type, this.name, url));
	}
```

是否启用hystrix配置处理类：
org.springframework.cloud.openfeign.FeignClientsConfiguration
###动态代理类长成什么样呢？
在启动是指定参数 -Dsun.misc.ProxyGenerator.saveGeneratedFiles=true，可以保存生成的动态代理类，在debug时候生成的代理类为名称为com/sun/proxy/$Proxy101.class，在项目同级目录下找到对应的class文件反编译得到
```java
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public final class $Proxy101 extends Proxy implements FeignTest {
   private static Method m1;
   private static Method m2;
   private static Method m3;
   private static Method m0;

	//默认被代理对象为FeignInvocationHandler，如果启用了feign.hystrix.enabled=true，被代理对象变为HystrixInvocationHandler
   public $Proxy101(InvocationHandler var1) throws  {
      super(var1);
   }

   //接口的get方法，实际上调用了FeignInvocationHandler.invoke方法
   public final String get() throws  {
      try {
         return (String)super.h.invoke(this, m3, (Object[])null);
      } catch (RuntimeException | Error var2) {
         throw var2;
      } catch (Throwable var3) {
         throw new UndeclaredThrowableException(var3);
      }
   }

   static {
      try {
         m1 = Class.forName("java.lang.Object").getMethod("equals", Class.forName("java.lang.Object"));
         m2 = Class.forName("java.lang.Object").getMethod("toString");
         m3 = Class.forName("com.harris.feign_client.FeignTest").getMethod("get");
         m0 = Class.forName("java.lang.Object").getMethod("hashCode");
      } catch (NoSuchMethodException var2) {
         throw new NoSuchMethodError(var2.getMessage());
      } catch (ClassNotFoundException var3) {
         throw new NoClassDefFoundError(var3.getMessage());
      }
   }

//equals,toString,hashCode...
}

// Feign默认实现
// ReflectiveFeign$FeignInvocationHandler.class
static class FeignInvocationHandler implements InvocationHandler {
    private final Target target;
    private final Map<Method, MethodHandler> dispatch;

	// 构造函数,创建代理时候将方法以及对应的处理方式传入,MethodHandler
    FeignInvocationHandler(Target target, Map<Method, MethodHandler> dispatch) {
      this.target = checkNotNull(target, "target");
      this.dispatch = checkNotNull(dispatch, "dispatch for %s", target);
    }

   public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
      if ("equals".equals(method.getName())) {
        try {
          Object otherHandler =
              args.length > 0 && args[0] != null ? Proxy.getInvocationHandler(args[0]) : null;
          return equals(otherHandler);
        } catch (IllegalArgumentException e) {
          return false;
        }
      } else if ("hashCode".equals(method.getName())) {
        return hashCode();
      } else if ("toString".equals(method.getName())) {
        return toString();
      }
	//根据不同的方法名分法到不同的MethodHandler处理
      return dispatch.get(method).invoke(args);
    }
}

//Hystrix实现:
HystrixInvocationHandler
```