###JDK动态代理
JDK动态代理样例:
```java
public class JdkProxyTest {

	//定义一个接口
	public static interface UserInterface {
		public void saveUser();
	}
	//接口实现类
	public static class UserInterfaceImpl implements UserInterface {

		@Override
		public void saveUser() {
			System.out.println("user saved ");
		}
	}
	// 代理类,需实现InvocationHandler接口
	public static class ProxyClass implements InvocationHandler {
		private Object o;

		public void setO(Object o) {
			this.o = o;
		}
		
		//参数proxy为生成的代理类,方法为实际的执行方法,args为方法参数
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			//执行实际方法之前
			System.out.println("before " + method.getName());
			//执行实际的方法	
			Object object = method.invoke(o, args);
			//执行实际方法之后	
			System.out.println("after " + method.getName());
			return object;
		}

	}
	//测试用例
	public static void main(String[] args) {
		//创建实例,userInterface指向的是UserInterfaceImpl
		UserInterface userInterface = new UserInterfaceImpl();
		//自定义在方法前后添加业务逻辑
		ProxyClass proxyClass = new ProxyClass();
		proxyClass.setO(userInterface);
		//生成代理类, 代理类实现了UserInterface接口,实际生成的类名为: com.sun.proxy.$Proxy0.class
		userInterface = (UserInterface) Proxy.newProxyInstance(proxyClass.getClass().getClassLoader(), userInterface.getClass().getInterfaces(), proxyClass);
		//调用代理类方法,实际通过ProxyClass调用UserInterface的实现类UserInterfaceImpl的saveUser方法
		userInterface.saveUser();
	}
}
/**
运行结果:
before saveUser
user saved 
after saveUser
*/

```
在运行时添加运行参数: -Dsun.misc.ProxyGenerator.saveGeneratedFiles=true 将会在项目根目录下生成com.sun.proxy.$Proxy0类,可以用jd-gui反编译后得到:
由于JDK代理机制继承了java.lang.reflect.Proxy,因此不能对普通的类代理,只能代理实现了接口的类,未实现接口的类不能代理(不支持多继承).
```java
public final class $Proxy0 extends Proxy implements JdkProxyTest.UserInterface {
	private static Method m1;
	private static Method m3;
	private static Method m2;
	private static Method m0;
	
	//static方法块, 初始化4个方法,其中3个为Object的,分别为equals,toString,hashCode,另外一个是接口定义的方法UserInterface.saveUser
	static {
		try {
			m1 = Class.forName("java.lang.Object").getMethod("equals", new Class[] { Class.forName("java.lang.Object") });
			m3 = Class.forName("com.example.testtransactional.proxy.JdkProxyTest$UserInterface").getMethod("saveUser", new Class[0]);
			m2 = Class.forName("java.lang.Object").getMethod("toString", new Class[0]);
			m0 = Class.forName("java.lang.Object").getMethod("hashCode", new Class[0]);
		} catch (NoSuchMethodException localNoSuchMethodException) {
			throw new NoSuchMethodError(localNoSuchMethodException.getMessage());
		} catch (ClassNotFoundException localClassNotFoundException) {
			throw new NoClassDefFoundError(localClassNotFoundException.getMessage());
		}
	}
	// 构造函数: 传入InvocationHandler, 以上例子为ProxyClass的实例,InvocationHandler为父类Proxy的一个成员变量
	public $Proxy0(InvocationHandler paramInvocationHandler) {
		super(paramInvocationHandler);
	}

	//saveUser方法,实际是通过ProxyClass调用UserInterface的实现类UserInterfaceImpl的saveUser方法
	//h实例为ProxyClass,m3为UserInterface的方法
	public final void saveUser() {
		try {
			this.h.invoke(this, m3, null);
			return;
		} catch (Error | RuntimeException localError) {
			throw localError;
		} catch (Throwable localThrowable) {
			throw new UndeclaredThrowableException(localThrowable);
		}
	}
	//...equals,toString,hashCode方法
}
```

### java.lang.reflect.Proxy类
```java
public class Proxy implements java.io.Serializable {
	//代理类构造方法参数,就行生成的Proxy0.class有个构造函数参数是InvocationHandler
    private static final Class<?>[] constructorParams =
        { InvocationHandler.class };
	
	//代理类缓存,如果存在的话则不需重新生成.
    private static final WeakCache<ClassLoader, Class<?>[], Class<?>>
        proxyClassCache = new WeakCache<>(new KeyFactory(), new ProxyClassFactory());

    protected InvocationHandler h;
	
	// 构造函数,不能被其他类实例化
    private Proxy() {
    }

	//构造函数,代理子类初始化会传入
    protected Proxy(InvocationHandler h) {
        Objects.requireNonNull(h);
        this.h = h;
    }
}
```

####创建代理方法 java.lang.reflect.Proxy.newProxyInstance(ClassLoader, Class<?>[], InvocationHandler)
```java
public static Object newProxyInstance(ClassLoader loader,
                                      Class<?>[] interfaces,
                                      InvocationHandler h)
throws IllegalArgumentException{
	//...省略
	//先从缓存proxyClassCache获取对应的class,如果没有实时生成一个并加入到缓存中
	Class<?> cl = getProxyClass0(loader, intfs);
	//...省略
	//查找代理类的只有一个参数为InvocationHandler构造函数
	//调用构造函数实例化代理类
	final Constructor<?> cons = cl.getConstructor(constructorParams);
	return cons.newInstance(new Object[]{h});
}

//发现proxyClassCache没有的时候,会用proxyClassCache实例化时传入的ProxyClassFactory来创建
//ProxyClassFactory 是Proxy的一个内部类.
private static final class ProxyClassFactory
    implements BiFunction<ClassLoader, Class<?>[], Class<?>>{
	//生成的代理类名称
	private static final String proxyClassNamePrefix = "$Proxy";
	//代理类序号,从0开始递增
	private static final AtomicLong nextUniqueNumber = new AtomicLong();
	//apply方法,生成代理类
	public Class<?> apply(ClassLoader loader, Class<?>[] interfaces) {
		//....
		//proxyName默认是com.sun.proxy.$Proxy自增id,类修饰符accessFlags=public final
		byte[] proxyClassFile = ProxyGenerator.generateProxyClass(
                proxyName, interfaces, accessFlags);
		//将生成的字节流转换成class并加载.
		return defineClass0(loader, proxyName,
                                    proxyClassFile, 0, proxyClassFile.length);
	}
}
```

####class字节流生成逻辑sun.misc.ProxyGenerator.generateProxyClass(String, Class<?>[], int)
```java
public static byte[] generateProxyClass(final String s, final Class<?>[] array, final int n) {
	final byte[] generateClassFile = new ProxyGenerator(s, array, n).generateClassFile();
	// 将生成的class字节流保存到磁盘,通过参数  sun.misc.ProxyGenerator.saveGeneratedFiles =true配置
	if (ProxyGenerator.saveGeneratedFiles) {
		// 保存文件
	}
	return generateClassFile;
}

private byte[] generateClassFile() {
		// 默认生成hashCode,equals,toString方法
        this.addProxyMethod(ProxyGenerator.hashCodeMethod, Object.class);
        this.addProxyMethod(ProxyGenerator.equalsMethod, Object.class);
        this.addProxyMethod(ProxyGenerator.toStringMethod, Object.class);

		// 生成接口中所有的方法,会根据方法名+参数去重
        for (final Class<?> clazz : this.interfaces) {
            final Method[] methods = clazz.getMethods();
            for (int length2 = methods.length, j = 0; j < length2; ++j) {
                this.addProxyMethod(methods[j], clazz);
            }
        }
        final Iterator<List<ProxyMethod>> iterator = this.proxyMethods.values().iterator();
        while (iterator.hasNext()) {
            checkReturnTypes(iterator.next());
        }
        try {
			//生成构造函数,带一个参数的InvocationHandler的构造函数,生成调用父类Proxy的构造函数super(InvocationHandler h)
            this.methods.add(this.generateConstructor());
            final Iterator<List<ProxyMethod>> iterator2 = this.proxyMethods.values().iterator();
 			// 为每个方法创建一个字段,accessFlag=10, 即private static Method xxx;
			//创建方法,生成方法字节码为 h.invoke(this, Method, Object...);
            while (iterator2.hasNext()) {
                for (final ProxyMethod proxyMethod : iterator2.next()) {
                    this.fields.add(new FieldInfo(proxyMethod.methodFieldName, "Ljava/lang/reflect/Method;", 10));
                    this.methods.add(proxyMethod.generateMethod());
                }
            }
			// 生成静态代码块,static{},为所有的字段生成Class.forName(...).getMethod()方法
            this.methods.add(this.generateStaticInitializer());
        }
		//...
		//当前类
        this.cp.getClass(dotToSlash(this.className));
        //父类
		this.cp.getClass("java/lang/reflect/Proxy");
		// 接口个数
        final Class<?>[] interfaces2 = this.interfaces;
		// 接口索引
        for (int length3 = interfaces2.length, k = 0; k < length3; ++k) {
            this.cp.getClass(dotToSlash(interfaces2[k].getName()));
        }
        this.cp.setReadOnly();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final DataOutputStream dataOutputStream = new DataOutputStream(out);
        try {
			// 写class二进制
			// 魔数 -889275714=0xCAFEBABE
            dataOutputStream.writeInt(-889275714);
			// 副版本minor_version=0
            dataOutputStream.writeShort(0);
			// 主版本major_version=49,即jdk1.5以上
            dataOutputStream.writeShort(49);
			// 常量池
            this.cp.write(dataOutputStream);
			// 类修饰符
            dataOutputStream.writeShort(this.accessFlags);
			// 当前类名在常量池的索引
            dataOutputStream.writeShort(this.cp.getClass(dotToSlash(this.className)));
			// 父类在常量池的索引
            dataOutputStream.writeShort(this.cp.getClass("java/lang/reflect/Proxy"));
			// 接口数量
            dataOutputStream.writeShort(this.interfaces.length);
            final Class<?>[] interfaces3 = this.interfaces;
			// 每个接口在常量池的索引
            for (int length4 = interfaces3.length, l = 0; l < length4; ++l) {
                dataOutputStream.writeShort(this.cp.getClass(dotToSlash(interfaces3[l].getName())));
            }
			// 字段数量
            dataOutputStream.writeShort(this.fields.size());
            final Iterator<FieldInfo> iterator4 = this.fields.iterator();
			// 每个字段的修饰符,名字在常量池的索引以及描述符在常量池的索引
            while (iterator4.hasNext()) {
                iterator4.next().write(dataOutputStream);
            }
			// 方法数量
            dataOutputStream.writeShort(this.methods.size());
            final Iterator<MethodInfo> iterator5 = this.methods.iterator();
			// 生成每个方法
            while (iterator5.hasNext()) {
                iterator5.next().write(dataOutputStream);
            }
            dataOutputStream.writeShort(0);
        }
        catch (IOException cause2) {
            throw new InternalError("unexpected I/O Exception", cause2);
        }
        return out.toByteArray();
    }
```
生成构造函数
```java
/*最终生成的构造函数为:
public 类名(InvocationHandler h){
	super(h);
}
*/

    private MethodInfo generateConstructor() throws IOException {
		//创建一个方法,方法名为<init>,描述符为(Ljava/lang/reflect/InvocationHandler;)V,accessFlag为1(public)
        final MethodInfo methodInfo = new MethodInfo("<init>", "(Ljava/lang/reflect/InvocationHandler;)V", 1);
        final DataOutputStream dataOutputStream = new DataOutputStream(methodInfo.code);
		// 将第一个局部变量入栈, 为this
        this.code_aload(0, dataOutputStream);
		// 将第二个局部变量入栈,为InvocationHandler
        this.code_aload(1, dataOutputStream);
		// 执行invokespecial指令,后面跟的方法是Proxy的构造函数,参数是InvocationHandler,将栈顶作为参数传递,也就是当前构造函数的InvocationHandler参数
        dataOutputStream.writeByte(183);
        dataOutputStream.writeShort(this.cp.getMethodRef("java/lang/reflect/Proxy", "<init>", "(Ljava/lang/reflect/InvocationHandler;)V"));
		// 执行return指令
        dataOutputStream.writeByte(177);
		// 最大栈深度
        methodInfo.maxStack = 10;
		// 局部变量个数
        methodInfo.maxLocals = 2;
		// 无异常情况
        methodInfo.declaredExceptions = new short[0];
        return methodInfo;
    }
```

可以看出JDK生成的代理类时通过写字节流生成的class文件,按照class文件格式生成.
