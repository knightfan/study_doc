##AbstractQueuedSynchronizer
java.util.concurrent包中很多类都依赖于这个类所提供队列式同步器，比如说常用的ReentranLock，Semaphore和CountDownLatch等,实现了FIFO的等待队列,提供了state(int类型)变量控制资源数量,之类必须保证state操作的线程安全.

###1. AbstractQueuedSynchronizer数据结构
```java
public abstract class AbstractQueuedSynchronizer
    extends AbstractOwnableSynchronizer{
	//内部类Node	 
	static final class Node{}
	//内部类ConditionObject
	public class ConditionObject implements Condition, java.io.Serializable {}

	//同步队列的头节点，通过setHead方法配置
    private transient volatile Node head;
	//同步队列的尾节点
    private transient volatile Node tail;
    //资源的可用数量
	private volatile int state;
}
```
###2. 内部类Node数据结构,同步队列中的节点信息
waitStatus保存队列中的状态，大于0的表示取消状态，小于0的表示等待，0初始值不做处理。
```java
static final class Node {
        /** Marker to indicate a node is waiting in shared mode */
		//等待共享锁
        static final Node SHARED = new Node();
        /** Marker to indicate a node is waiting in exclusive mode */
		//等待排他锁        
		static final Node EXCLUSIVE = null;

        /** waitStatus value to indicate thread has cancelled */
		//由于当前节点等待超时或被中断，当前线程已取消获取资源，不再阻塞
        static final int CANCELLED =  1;
        /** waitStatus value to indicate successor's thread needs unparking */
		//标识当前线程所在的节点需唤醒同步队列中的后继节点运行，调用后继节点的线程的unpark方法
        static final int SIGNAL    = -1;
        /** waitStatus value to indicate thread is waiting on condition */
        //当前线程在等待condition条件产生
		static final int CONDITION = -2;
        /**
         * waitStatus value to indicate the next acquireShared should
         * unconditionally propagate
         */
		//
        static final int PROPAGATE = -3;
		
		//资源等待状态，分别为以上定义的常量
		//1. CANCELLED，由于等待超时或被中断(抛出异常)，当前节点的前驱节点(非CANCELLED)被设置为CANCELLED
		//2. SIGNAL当前节点在释放资源时，必须唤醒当前节点的后继节点，后继节点在尝试获取资源时被阻塞(park)，后继结点在被唤醒(unpark)后要重新获取资源(tryAcquire),如果获取失败，需被阻塞。
		//3. CONDITION表示当前节点在等待队列中
		//4. PROPAGATE在释放共享资源(releaseShared)时传播到其他节点(唤醒所有的同步队列中的线程)，只设置在头节点中。
		//5. 0 
        volatile int waitStatus;

       //前驱节点
        volatile Node prev;

       //后继节点
        volatile Node next;

		//线程对象
        volatile Thread thread;
		
		//在condition队列中的后继节点,或者是共享节点。
        Node nextWaiter;
    }
```
###3. 内部类ConditionObject数据结构，主要实现等待队列，队列的节点信息也是Node
```java
public class ConditionObject implements Condition, java.io.Serializable {
       //等待队列的头节点
        private transient Node firstWaiter;
        //等待队列的尾节点
        private transient Node lastWaiter;
}
```

###4. 获取独占资源
####4.1 acquire(int)
acquire调用tryAcquire尝试获取独占资源，如果失败将加入同步队列中
```java
    public final void acquire(int arg) {
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
			//如果获取锁失败，并且在同步队列中被设置中断标志位，则清除当前线程中断标志位，然后自行调用中断。
			//Thread.currentThread().interrupt();
            selfInterrupt();
    }
```
####4.2 tryAcquire(int)
尝试获取资源，如果获取成功返回true，失败返回false，获取之前应该查询state可用数量,更新state值
```java
    protected boolean tryAcquire(int arg) {
        throw new UnsupportedOperationException();
    }
```
####4.3 addWaiter(Node)
将节点加入同步队列中，先尝试在尾节点加入，失败的话再循环尝试CAS插入尾节点，至此，加入同步队列完成。
```java
  private Node addWaiter(Node mode) {
        Node node = new Node(Thread.currentThread(), mode);
        // Try the fast path of enq; backup to full enq on failure
        Node pred = tail;
        if (pred != null) {
            node.prev = pred;
            if (compareAndSetTail(pred, node)) {
                pred.next = node;
                return node;
            }
        }
        enq(node);
        return node;
    }
	//普通插入同步队列尾节点尝试通过CAS修改尾节点
  private Node enq(final Node node) {
        for (;;) {
            Node t = tail;
            if (t == null) { // Must initialize
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {
                node.prev = t;
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    return t;
                }
            }
        }
    }
```
####4.4 acquireQueued(final Node node, int arg)
acquireQueued对于已加入到同步队列的节点,判断是否要阻塞当前线程或当前线程被中断,如果出现异常,如被中断,将当前节点的waitStatus设置为CANCELLED状态,返回当前线程是否被设置中断标志位。
```java
 final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (;;) {
				//获取独占资源模式下，只有前驱为头节点时才会尝试获取资源，获取到资源会将当前节点设置为头节点,先前的头节点会被出队列回收.
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
				//判断是否应该阻塞当前线程，
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
			//出现异常，将当前节点waitStatus设置为CANCELLED，并移出同步队列
            if (failed)
                cancelAcquire(node);
        }
    }
```
####4.5 shouldParkAfterFailedAcquire

1. 如果前驱节点的waitStatus是SIGNAL，则可以安全进入阻塞
2. 找到前驱节点是CANCELLED的节点，将往前找到的第一个不是CANCELLED前驱节点的next设置为当前节点,同时释放在当前节点与前驱节点之间的所有元素.
3. 如果前驱节点的waitStatus是0或PROPAGATE，将前驱节点的waitStatus设置为SIGNAL，并重试一次获取资源，调用方不断在循环尝试，找到park的安全点进入等待状态(前驱节点状态waitStatus为SIGNAL)，线程进入TIME_WAITING状态
```java
//判断当前线程是否应该阻塞
 private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
        int ws = pred.waitStatus;
        if (ws == Node.SIGNAL)
            /*
             * This node has already set status asking a release
             * to signal it, so it can safely park.
             */
            return true;
        if (ws > 0) {
            /*
             * Predecessor was cancelled. Skip over predecessors and
             * indicate retry.
             */
            do {
                node.prev = pred = pred.prev;
            } while (pred.waitStatus > 0);
            pred.next = node;
        } else {
            /*
             * waitStatus must be 0 or PROPAGATE.  Indicate that we
             * need a signal, but don't park yet.  Caller will need to
             * retry to make sure it cannot acquire before parking.
             */
            compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
        }
        return false;
    }
```
####4.6 parkAndCheckInterrupt
阻塞当前线程，被唤醒之后检查并清除中断标志位。
```java
    private final boolean parkAndCheckInterrupt() {
        LockSupport.park(this);
        return Thread.interrupted();
    }
```
###5. 释放资源
#### release(int arg)
释放独占资源，如果释放成功，同步队列中有节点的话，尝试唤醒头节点的后继关联的线程
```java
    public final boolean release(int arg) {
   		if (tryRelease(arg)) {
            Node h = head;
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);
            return true;
        }
        return false;
    }
	//释放独占资源，成功返回true，否则返回false，由子类实现
    protected boolean tryRelease(int arg) {
        throw new UnsupportedOperationException();
    }
````
####unparkSuccessor(Node node)
唤醒当前节点后继节点的线程,从队列尾节点往前找到一个最近的waitStatus小于0(可以是SIGNAL或PROPAGATE)的节点(跳过被取消的节点)，唤醒这个节点关联的线程，传入的头节点参数有可能为cancelled或者null，则从tail节点开始倒着查找。
```java
	private void unparkSuccessor(Node node) {
        /*
         * If status is negative (i.e., possibly needing signal) try
         * to clear in anticipation of signalling.  It is OK if this
         * fails or if status is changed by waiting thread.
         */
        int ws = node.waitStatus;
        if (ws < 0)
            compareAndSetWaitStatus(node, ws, 0);

        /*
         * Thread to unpark is held in successor, which is normally
         * just the next node.  But if cancelled or apparently null,
         * traverse backwards from tail to find the actual
         * non-cancelled successor.
         */
        Node s = node.next;
        if (s == null || s.waitStatus > 0) {
            s = null;
            for (Node t = tail; t != null && t != node; t = t.prev)
                if (t.waitStatus <= 0)
                    s = t;
        }
        if (s != null)
            LockSupport.unpark(s.thread);
    }
```
### 5.获取共享资源
共享资源如读写锁，写独占，读共享，在写的过程中获取独占锁，不能读或者写，如果有正在读或写的线程，需等待其执行完。在读的过程中，其他线程可以读，但不能写。
####acquireShared 
获取共享资源，tryAcquireShared由子类实现，注意跟独占资源不一样的是返回值不是boolean，是整数(负数表示资源不可用，需进入等待队列，0表示当前线程获取完之后没有可用资源，正数表示获取完资源后还剩余的资源数)，在继承这个抽象类的时候注意返回值。如果获取失败，则加入同步队列中，直到其他线程唤醒。
```java
    public final void acquireShared(int arg) {
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
```
####doAcquireShared	获取共享资源
获取共享资源失败，加入同步队列中，(park方法中,interrupt会唤醒线程但不会抛出InterruptedException)在被唤醒后发现被设置中断标志位，则清除中断标志位并继续执行，如果当前节点的前驱是头结点(头结点没有任何信息，只是个空节点，有唤醒标志)，标识前面没有等待的线程了，可以尝试获取资源，获取到资源后，如果后继节点也是共享模式，则唤醒后继节点，所有的共享节点可以并发执行。
```java
 private void doAcquireShared(int arg) {
		//加入同步队列中
        final Node node = addWaiter(Node.SHARED);
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (;;) {
				//只有头结点才能获取资源
                final Node p = node.predecessor();
                if (p == head) {
					//tryAcquireShared获取共享资源，如果返回负数表示获取失败，返回0表示成功但没有可用资源，正数表示有可用资源。
                    int r = tryAcquireShared(arg);
                    if (r >= 0) {
						//设置完头结点后，如果下一节点也是SHARE模式的话，传播唤醒下一节点
                        setHeadAndPropagate(node, r);
                        p.next = null; // help GC
						//如果在阻塞过程中被中断了，清除中断标志位。
                        if (interrupted)
                            selfInterrupt();
                        failed = false;
                        return;
                    }
                }
				//如果前驱不是头结点或者获取资源失败了，判断是否要阻塞当前线程，与独占资源判断逻辑一致。如果要阻塞，判断中断标识。
                if (shouldParkAfterFailedAcquire(p, node) &&
                    parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }
```
####setHeadAndPropagate 传播共享节点
将当前节点设置头结点，如果下一节点也是共享模式，唤醒并传播下一节点，直到为空或不是共享节点为止
```java
    private void setHeadAndPropagate(Node node, int propagate) {
        Node h = head; // Record old head for check below
        setHead(node);
        /*
         * Try to signal next queued node if:
         *   Propagation was indicated by caller,
         *     or was recorded (as h.waitStatus either before
         *     or after setHead) by a previous operation
         *     (note: this uses sign-check of waitStatus because
         *      PROPAGATE status may transition to SIGNAL.)
         * and
         *   The next node is waiting in shared mode,
         *     or we don't know, because it appears null
         *
         * The conservatism in both of these checks may cause
         * unnecessary wake-ups, but only when there are multiple
         * racing acquires/releases, so most need signals now or soon
         * anyway.
         */
        if (propagate > 0 || h == null || h.waitStatus < 0 ||
            (h = head) == null || h.waitStatus < 0) {
            Node s = node.next;
            if (s == null || s.isShared())
				//传播到后继节点
                doReleaseShared();
        }
    }
```
####doReleaseShared 
释放共享资源
如果头结点waitStatus是SIGNAL的话，将SIGNAL(-1)更新成0，唤醒头结点下一节点线程(本身是SHARE模式，可以唤醒下一节点获取资源)，继续循环。
如果waitStatus没有设置过值(默认是0)，则设置为PROPAGATE模式。
```java
   private void doReleaseShared() {
        /*
         * Ensure that a release propagates, even if there are other
         * in-progress acquires/releases.  This proceeds in the usual
         * way of trying to unparkSuccessor of head if it needs
         * signal. But if it does not, status is set to PROPAGATE to
         * ensure that upon release, propagation continues.
         * Additionally, we must loop in case a new node is added
         * while we are doing this. Also, unlike other uses of
         * unparkSuccessor, we need to know if CAS to reset status
         * fails, if so rechecking.
         */
        for (;;) {
            Node h = head;
            if (h != null && h != tail) {
                int ws = h.waitStatus;
                if (ws == Node.SIGNAL) {
                    if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                        continue;            // loop to recheck cases
                    unparkSuccessor(h);
                }
                else if (ws == 0 &&
                         !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                    continue;                // loop on failed CAS
            }
            if (h == head)                   // loop if head changed
                break;
        }
    }
```
###6. cancelAcquire 取消资源获取
分3类情况
1.取消的节点node是尾节点，尾节点设置为当前节点的前驱，前驱的next指针置为null；
2.取消的节点node不是头结点的后继，则将node的前驱节点的next指向node的后继节点，此时node节点还不能被GC回收调，因为node的后继节点的pre还是指向node(还被后继节点引用)，最终由其他线程在调用cancelAcquire或shouldParkAfterFailedAcquire在跳过CANCEL节点时才会将引用断开，也就是node的后继节点的pre指向node的pre，这时候node才能被GC。
3.如果是头结点的next节点，唤醒当前节点的下一节点，让节点关联的线程从shouldParkAfterFailedAcquire中唤醒尝试重新获取资源。
```java 
   /**
     * Cancels an ongoing attempt to acquire.
     *
     * @param node the node
     */
    private void cancelAcquire(Node node) {
        // Ignore if node doesn't exist
        if (node == null)
            return;

        node.thread = null;

        // Skip cancelled predecessors
        Node pred = node.prev;
        while (pred.waitStatus > 0)
            node.prev = pred = pred.prev;

        // predNext is the apparent node to unsplice. CASes below will
        // fail if not, in which case, we lost race vs another cancel
        // or signal, so no further action is necessary.
        Node predNext = pred.next;

        // Can use unconditional write instead of CAS here.
        // After this atomic step, other Nodes can skip past us.
        // Before, we are free of interference from other threads.
        node.waitStatus = Node.CANCELLED;

        // If we are the tail, remove ourselves.
		// 尾节点
        if (node == tail && compareAndSetTail(node, pred)) {
            compareAndSetNext(pred, predNext, null);
        } else {
			// 不是头结点的后继节点
            // If successor needs signal, try to set pred's next-link
            // so it will get one. Otherwise wake it up to propagate.
            int ws;
            if (pred != head &&
                ((ws = pred.waitStatus) == Node.SIGNAL ||
                 (ws <= 0 && compareAndSetWaitStatus(pred, ws, Node.SIGNAL))) &&
                pred.thread != null) {
                Node next = node.next;
                if (next != null && next.waitStatus <= 0)
                    compareAndSetNext(pred, predNext, next);
            } else {
				//头结点的下一节点，尝试唤醒下一线程重新获取资源。
                unparkSuccessor(node);
            }

            node.next = node; // help GC
        }
    }
```

##内部类 ConditionObject实现


public class ConditionObject implements Condition, java.io.Serializable {
}