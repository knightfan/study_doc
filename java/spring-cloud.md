##Spring Cloud
服务注册中心(eureka,zk)，配置中心，服务发现注册，断路器(限流保护)，路由策略，负载均衡，全局锁，分布式会话，客户端调用，接口网关(Zuul)，服务管理系统，健康检查。

Consistency(一致性)，它要求在同一时刻点，分布式系统中的所有数据备份都处于同一状态。
Availability(可用性)，在系统集群的一部分节点宕机后，系统依然能够响应用户的请求。
Partition Tolerance(分区容错性)，在网络区间通信出现失败，系统能够容忍。
一般来讲，基于网络的不稳定性，分布容错是不可避免的，所以我们默认CAP中的P总是成立的。

一致性的强制数据统一要求，必然会导致在更新数据时部分节点处于被锁定状态，此时不可对外提供服务，影响了服务的可用性，反之亦然。因此一致性和可用性不能同时满足。

服务注册和发现组件中，Eureka满足了其中的AP，Consul和Zookeeper满足了其中的CP。


###spring cloud 集成注册中心
eureka:netfilx出品，满足AP，即能容忍集群中某些时刻数据不一致情况。但是某些节点挂了并不会影响服务
consul，满足CP，使用raft(至少有一半以上节点同步成功数据)保证数据一致性，leader挂了之后集群进入选举模式，期间不能对外服务，需等leader选举出来后才能开始对外服务。
zookeeper，zab协议实现，情况与consul一样。

raft相关说明：
https://www.jianshu.com/p/8e4bbe7e276c
http://thesecretlivesofdata.com/raft/

raft 与zab 区别
https://my.oschina.net/pingpangkuangmo/blog/782702

Consul特性：
服务发现：提供服务注册和服务发现接口
健康检查：与服务定时保持通信，一旦发现服务故障则可告警下线，防止其他服务访问到故障的服务。
KV存储：用来存储动态配置的系统，提供简单的HTTP访问方式。
多数据中心：无需复杂的配置，即可支持任意数量的区域。


###配置中心:Apollo
配置特性:
1. 独立于程序的只读变量,同一个程在不同的配置下有不同的行为,程序通过配置变量改变自身的行为.
2. 配置伴随应用的整个生命周期.
3. 多种方式加载配置,如hard code,配置文件,启动参数,数据库配置等等.
4. 配置治理,权限控制;环境(开发,测试,生产)、集群配置(不同的数据中心)管理;框架类组件配置管理 
Apollo解决的问题:
1. 统一管理不同环境(environment)、不同集群(cluster),不同命名空间(namespace)的配置,通过命名空间方便不同的应用共享一份配置.
2. 配置修改实时生效（热发布）,配置发布后可以通知应用程序变更.
3. 版本发布管理,配置发布后生成版本快照,以便回滚.
4. 灰度发布,配置变更后支持部分应用受到配置更新通知,待验证没问题就在通知到所有的应用.
5. 权限管理、发布审核、操作审计
6. 客户端配置信息监控,界面方便管理配置
7. 提供Java和.Net原生客户端
8. 提供开放平台API
9. 部署简单,jar包,依赖jdk与mysql即可.

###服务保护Hystrix
停更,替换:Alibaba Sentinel,Resilience4J
对网络故障，延迟的服务进行保护与控制。
分布式系统中防止级联故障，雪崩效应。
快速失败，快速恢复。
失败回退，尽可能优雅降级。
监控，告警。

实现方式：
每个服务依赖维护一个小的线程池(或信号量)，如果线程池满了(或信号量不可用)，则拒绝请求，执行fallback逻辑，每个服务依赖互不影响。
在一个时间段内，调用服务失败百分比超过一定的阈值(缺省是5秒内20次调用失败就会启动熔断机制)，会触发断路器来停止对特定的服务请求(直接执行本地fallback逻辑，不执行远程调用)，防止自身被影响。
熔断器3种状态： open(开启)，half-open(半开启)，close(关闭)
状态转换：
close->open，在一个时间段内，调用服务失败百分比超过一定的阈值，会触发变成开启状态open，关闭远程调用。
open->half-open，熔断器开启后，在一个时间窗口内，会试探性开启部分远程调用，如果能正常返回则逐渐恢复，最终变成close，如果还是失败的话会变成open，后续再一个新的时间窗口继续试探。
half-open->close，如果在试探的过程中都能正常返回，则熔断器将变成close，将执行远程调用。

更多详细配置,参考链接:Hystrix Configuration
[https://github.com/Netflix/Hystrix/wiki/Configuration](https://github.com/Netflix/Hystrix/wiki/Configuration "Hystrix Configuration")


###客户端负载均衡Ribbon
依赖包spring-cloud-consul-discovery中的spring.factories中配置了RibbonConsulAutoConfiguration,这个类是配置表示consul,从consul拉取服务器列表,选择一个合适的实例调用,失败有重试机制.
ILoadBalancer 负载均衡器
IRule 负责均衡策略
IPing 检查实例是否可用
IClientConfig 客户端配置
ServerList 服务器列表
ServerListFilter 服务器过滤规则

ribbon默认配置位于com.netflix.client.config.DefaultClientConfigImpl。
配置格式为：<clientName>.<nameSpace>.<propertyName>=<value>，如service1.ribbon.ReadTimeout=100,如果没有指定client名称,则对所有的client都有效.
配置样例:
```properties
ribbon.ConnectTimeout = 200 #请求连接的超时时间。
ribbon.ReadTimeout = 1000 #请求处理的超时时间。
ribbon.OkToRetryOnAllOperations = true #对所有操作请求都进行重试。
ribbon.MaxAutoRetriesNextServer = 2 #切换实例的重试次数。
ribbon.MaxAutoRetries = 2 #对当前实例的重试次数。
```

###网关路由zuul
zuul 是所有请求(应用或web)到后台微服务的中间层,所有的web请求经过zuul后再调用相关的微服务.
zuul通过一系列的Filter来控制前端请求,提供的功能有:
1. 安全与认证,识别合法请求,拦截非法请求.
2. 监控,监控一些有意义的数据,提供可视化的监控界面.
3. 动态路由,按需动态路由到后台微服务.
4. 压测,逐渐增加流量以测试集群的性能.
5. 丢弃负载,为每个请求分配合适的容量,当达到最大时候丢弃.
6. 静态资源相应,静态资源可以直接在网关返回,不需要转发到后端集群.
7. 多区域与弹性扩容,

#####ZuulFilter过滤器

```java
public abstract class ZuulFilter implements IZuulFilter, Comparable<ZuulFilter>{
	//过滤器类型,分别为:
	//pre:在请求被路由（转发）之前调用
	//route:在路由（请求）转发时被调用
	//error:服务网关发生异常时被调用
	//post:在路由（转发）请求后调用
	abstract public String filterType();
	//过滤器优先级排序
	abstract public int filterOrder();
	//IZuulFilter方法, 返回true表示应该执行run方法
	boolean shouldFilter();
	//IZuulFilter方法, 过滤器执行方法
	Object run();

//每个Filter都会执行runFilter方法,该方法返回ZuulFilterResult,返回值包括result,exception,status
 public ZuulFilterResult runFilter() {
        ZuulFilterResult zr = new ZuulFilterResult();
		//是否取消了过滤isFilterDisabled,默认是false
        if (!isFilterDisabled()) {
            if (shouldFilter()) {
                Tracer t = TracerFactory.instance().startMicroTracer("ZUUL::" + this.getClass().getSimpleName());
                try {
					//正常执行结束,返回结果,状态为success
                    Object res = run();
                    zr = new ZuulFilterResult(res, ExecutionStatus.SUCCESS);
                } catch (Throwable e) {
					//执行失败,状态为failed,返回异常信息
                    t.setName("ZUUL::" + this.getClass().getSimpleName() + " failed");
                    zr = new ZuulFilterResult(ExecutionStatus.FAILED);
                    zr.setException(e);
                } finally {
                    t.stopAndLog();
                }
            } else {
				//返回为跳过
                zr = new ZuulFilterResult(ExecutionStatus.SKIPPED);
            }
        }
        return zr;
    }
}
```
#####Zuul架构图
![Zuul架构图](pic/ZuulArchitectural.png)

从网络过来的请求会经过pre-filters (inbound filters),然后经过Endpoint Filters(返回静态资源,处理目标请求),经过post-filters (outbound filters)返回给客户端.
Filter特性:
1. Type：用以表示路由过程中的阶段（内置包含PRE、ROUTING、POST和ERROR）
2. Async,定义是否异步执行的Filter
3. Execution Order：表示相同Type的Filter的执行顺序
4. Criteria：执行条件
5. Action：满足条件的执行体

zuul与nginx区别:
Zuul和Nginx都可以实现负载均衡、反向代理、过滤请求、实现网关效果。
zuul是java实现,微服务的负载均衡(通过服务治理中心+ribbon),nginx以C语言实现服务端配置负载均衡

