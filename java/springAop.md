#spring Aop

##启动方式
在spring-boot-autoconfigure包下/META-INF/spring.factories的其中一个配置:
```properties
# Auto Configure
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
...
org.springframework.boot.autoconfigure.aop.AopAutoConfiguration,\
...
```
在Spring boot启动过程中@SpringBootApplication(标注了@EnableAutoConfiguration)会加载以上配置,处理类是org.springframework.boot.autoconfigure.AutoConfigurationImportSelector.AutoConfigurationGroup.process(AnnotationMetadata, DeferredImportSelector)

###AopAutoConfiguration定义
ConditionalOnProperty表示如果配置存在spring.aop.auto=true启用启用配置类,没有配置的话默认是true
1. org.aspectj.weaver.Advice在当前类路径时,注册的beanName:
org.springframework.aop.config.internalAutoProxyCreator=AnnotationAwareAspectJAutoProxyCreator
1.1 仅当存在配置 spring.aop.proxy-target-class=false 启用JDK动态代理,EnableAspectJAutoProxy属性proxyTargetClass赋值为false
1.2 存在配置spring.aop.proxy-target-class=true,或缺失此配置时,启用CGLIB动态代理,EnableAspectJAutoProxy属性proxyTargetClass赋值为false
JDK动态代理与CGLIB动态代理主要区别是,JDK只能代理接口,对于未实现接口的类不能代理,CGLIB则可以代理接口或类.
2. org.aspectj.weaver.Advice不在当前类路径时,注册的beanName:
org.springframework.aop.config.internalAutoProxyCreator=InfrastructureAdvisorAutoProxyCreator

```java
@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(prefix = "spring.aop", name = "auto", havingValue = "true", matchIfMissing = true)
public class AopAutoConfiguration {

	//存在org.aspectj.weaver.Advice使用此配置
	@Configuration(proxyBeanMethods = false)
	@ConditionalOnClass(Advice.class)
	static class AspectJAutoProxyingConfiguration {
		
		//仅当存在配置 spring.aop.proxy-target-class=false是启用JDK动态代理,EnableAspectJAutoProxy属性proxyTargetClass赋值为false
		@Configuration(proxyBeanMethods = false)
		@EnableAspectJAutoProxy(proxyTargetClass = false)
		@ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "false",
				matchIfMissing = false)
		static class JdkDynamicAutoProxyConfiguration {

		}

		//存在配置spring.aop.proxy-target-class=true,或缺失此配置时,启用CGLIB动态代理,EnableAspectJAutoProxy属性proxyTargetClass赋值为true
		@Configuration(proxyBeanMethods = false)
		@EnableAspectJAutoProxy(proxyTargetClass = true)
		@ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "true",
				matchIfMissing = true)
		static class CglibAutoProxyConfiguration {

		}

	}
	
	//不存在org.aspectj.weaver.Advice则注册InfrastructureAdvisorAutoProxyCreator来代理
	@Configuration(proxyBeanMethods = false)
	@ConditionalOnMissingClass("org.aspectj.weaver.Advice")
	@ConditionalOnProperty(prefix = "spring.aop", name = "proxy-target-class", havingValue = "true",
			matchIfMissing = true)
	static class ClassProxyingConfiguration {

		ClassProxyingConfiguration(BeanFactory beanFactory) {
			if (beanFactory instanceof BeanDefinitionRegistry) {
				BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
				AopConfigUtils.registerAutoProxyCreatorIfNecessary(registry);
				AopConfigUtils.forceAutoProxyCreatorToUseClassProxying(registry);
			}
		}

	}
}

```

AnnotationAwareAspectJAutoProxyCreator,InfrastructureAdvisorAutoProxyCreator 这两个类关系:
```
// Class hierarchy
// - Object
//   - ProxyConfig
//     - ProxyProcessorSupport
//       - AbstractAutoProxyCreator
//         - AbstractAdvisorAutoProxyCreator
//           - InfrastructureAdvisorAutoProxyCreator
//           - AspectJAwareAdvisorAutoProxyCreator
//             - AnnotationAwareAspectJAutoProxyCreator
//           - DefaultAdvisorAutoProxyCreator
```

实现了SmartInstantiationAwareBeanPostProcessor,也就是一个BeanPostProcessor,在refreshContext时候注册到DefaultListableBeanFactory(AbstractApplicationContext.registerBeanPostProcessors),在创建bean时候调BeanPostProcessor前后置方法来处理bean,此处可以在bean实例化之后(属性注入完成,初始化方法执行完成)调用applyBeanPostProcessorsAfterInitialization来增强bean.

```java
//InfrastructureAdvisorAutoProxyCreator的父类
//AbstractAutoProxyCreator.postProcessAfterInitialization
	public Object postProcessAfterInitialization(@Nullable Object bean, String beanName) {
		if (bean != null) {
			//获取缓存key,如果是FactoryBean则&+beanName,否则为bean本身.
			Object cacheKey = getCacheKey(bean.getClass(), beanName);
			 //判断是否这个bean已经被代理过,代理过的不需再代理
			if (this.earlyProxyReferences.remove(cacheKey) != bean) {
				return wrapIfNecessary(bean, beanName, cacheKey);
			}
		}
		return bean;
	}
```
wrapIfNecessary是否需要代理(对应的Bean是否有满足条件的Advisors),需要的话创建代理
```java
	//wrapIfNecessary
	protected Object wrapIfNecessary(Object bean, String beanName, Object cacheKey) {
	    //如果已经处理过,在创建bean之前加入targetSourcedBeans表示不需要代理，直接返回bean
		if (StringUtils.hasLength(beanName) && this.targetSourcedBeans.contains(beanName)) {
			return bean;
		}
		//不需要代理,
		if (Boolean.FALSE.equals(this.advisedBeans.get(cacheKey))) {
			return bean;
		}
		//是否基类,实现了(Advice,Pointcut,Advisor,AopInfrastructureBean)视为基类,基类不需被代理
		//或者配置了指定bean不需要自动代理(初始化现有bean实例时“原始实例”约定的后缀.ORIGINAL),记录不需要代理的bean
		if (isInfrastructureClass(bean.getClass()) || shouldSkip(bean.getClass(), beanName)) {
			this.advisedBeans.put(cacheKey, Boolean.FALSE);
			return bean;
		}

		// Create proxy if we have advice.
		// 获取合适的Advisor Bean,如果有的话,则创建代理
		Object[] specificInterceptors = getAdvicesAndAdvisorsForBean(bean.getClass(), beanName, null);
		if (specificInterceptors != DO_NOT_PROXY) {
			this.advisedBeans.put(cacheKey, Boolean.TRUE);
			//创建代理bean
			Object proxy = createProxy(
					bean.getClass(), beanName, specificInterceptors, new SingletonTargetSource(bean));
			this.proxyTypes.put(cacheKey, proxy.getClass());
			return proxy;
		}
		//不需创建代理
		this.advisedBeans.put(cacheKey, Boolean.FALSE);
		return bean;
	}


	//查找Bean对应的Advisor
	protected Object[] getAdvicesAndAdvisorsForBean(
			Class<?> beanClass, String beanName, @Nullable TargetSource targetSource) {
		//查找合适bean的Advisor
		List<Advisor> advisors = findEligibleAdvisors(beanClass, beanName);
		if (advisors.isEmpty()) {
			return DO_NOT_PROXY;
		}
		return advisors.toArray();
	}
	
	protected List<Advisor> findEligibleAdvisors(Class<?> beanClass, String beanName) {
		//获取全部Advisor
		List<Advisor> candidateAdvisors = findCandidateAdvisors();
		// 过滤出合适的Advisor,分两类Advisor
		// 1. IntroductionAdvisor,满足ClassFilter.matches
		// 2. PointcutAdvisor,满足Pointcut定义的方法,根据切点表达式来判断
		List<Advisor> eligibleAdvisors = findAdvisorsThatCanApply(candidateAdvisors, beanClass, beanName);
        //如果代理类集合不为空的时候，检查所有代理类中是不是包含AspectJ（AspectJ表达式）代理，如果包含则需要把ExposeInvocationInterceptor类作为连接器链的第一个
		extendAdvisors(eligibleAdvisors);
		// Advisor排序
		if (!eligibleAdvisors.isEmpty()) {
			eligibleAdvisors = sortAdvisors(eligibleAdvisors);
		}
		return eligibleAdvisors;
	}
```

####多个Advisor
如以下代码加了异步处理与事务注解,生成的代理类有两个Advisor,Async注解的处理类将Async的Advisor放在第一个位置,被代理方法不能是static或final.
```java
	public class TransactionService{
		@Transactional
		@Async
		public void doInTransaction() {
		}
	}
	//AsyncAnnotationBeanPostProcessor初始化时将Async处理放在首位
	public AsyncAnnotationBeanPostProcessor() {
		setBeforeExistingAdvisors(true);
	}
```
![aop生成的实例对象](pic/aop_instant.png)
如图可以看到有个static final修饰的变量名 
CGLIB$doInTransaction$0$Method,值为实际执行的方法doInTransaction
CGLIB$doInTransaction$0$Proxy 为生成的代理类对方法的代理引用
cglib$CALLBACK_0为方法拦截器DynamicAdvisedInterceptor
CGLIB$CALLBACK_3为StaticDispatcher,CGLIB$CALLBACK_4为AdvisedDispatcher

Callback接口:
All callback interfaces used by {@link Enhancer} extend this interface. 
MethodInterceptor  方法拦截器
NoOp 不进行拦截
LazyLoader 适用于被代理对象需要懒加载的场景
Dispatcher 每次调用都会重新加载被代理的对象
ProxyRefDispathcher 与Dispatcher类似，但是它支持传入代理对象
InvocationHandler 
FixedValue 回调方法返回固定值

执行过程:
在启动虚拟机执行时候设置变量:
```java
System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\cglib");  
```
则执行过程中会保存CGLIB生成的class文件,该类继承了TransactionService,实现了 SpringProxy, Advised, Factory接口
```java
public class TransactionService$$EnhancerBySpringCGLIB$$5c62f07e extends TransactionService implements SpringProxy, Advised, Factory
{
private boolean CGLIB$BOUND;
    public static Object CGLIB$FACTORY_DATA;
    private static final ThreadLocal CGLIB$THREAD_CALLBACKS;
    private static final Callback[] CGLIB$STATIC_CALLBACKS;
	//Callback
    private MethodInterceptor CGLIB$CALLBACK_0;
    private MethodInterceptor CGLIB$CALLBACK_1;
    private NoOp CGLIB$CALLBACK_2;
    private Dispatcher CGLIB$CALLBACK_3;
    private Dispatcher CGLIB$CALLBACK_4;
    private MethodInterceptor CGLIB$CALLBACK_5;
    private MethodInterceptor CGLIB$CALLBACK_6;

    private static Object CGLIB$CALLBACK_FILTER;
    private static final Method CGLIB$doInTransaction$0$Method;
    private static final MethodProxy CGLIB$doInTransaction$0$Proxy;
    private static final Object[] CGLIB$emptyArgs;
    private static final Method CGLIB$equals$1$Method;
    private static final MethodProxy CGLIB$equals$1$Proxy;
    private static final Method CGLIB$toString$2$Method;
    private static final MethodProxy CGLIB$toString$2$Proxy;
    private static final Method CGLIB$hashCode$3$Method;
    private static final MethodProxy CGLIB$hashCode$3$Proxy;
    private static final Method CGLIB$clone$4$Method;
    private static final MethodProxy CGLIB$clone$4$Proxy;

    public final void doInTransaction() {
        MethodInterceptor cglib$CALLBACK_2;
        MethodInterceptor cglib$CALLBACK_0;
		//如果没有绑定方法拦截器,先绑定方法拦截器,截图里的方法拦截器是DynamicAdvisedInterceptor
        if ((cglib$CALLBACK_0 = (cglib$CALLBACK_2 = this.CGLIB$CALLBACK_0)) == null) {
            CGLIB$BIND_CALLBACKS(this);
            cglib$CALLBACK_2 = (cglib$CALLBACK_0 = this.CGLIB$CALLBACK_0);
        }
		//调用方法拦截器的intercept,这里是DynamicAdvisedInterceptor.intercept
		//参数:
		//proxy代理对象
		//method被代理对象的实际方法(原始方法)
		//args方法参数
		//methodProxy为生成的代理类对方法的代理引用
        if (cglib$CALLBACK_0 != null) {
            cglib$CALLBACK_2.intercept((Object)this, TransactionService$$EnhancerBySpringCGLIB$$5c62f07e.CGLIB$doInTransaction$0$Method, TransactionService$$EnhancerBySpringCGLIB$$5c62f07e.CGLIB$emptyArgs, TransactionService$$EnhancerBySpringCGLIB$$5c62f07e.CGLIB$doInTransaction$0$Proxy);
            return;
        }
        super.doInTransaction();
    }
}
```


####DynamicAdvisedInterceptor.intercept
```java
	public Object intercept(Object proxy, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
			Object oldProxy = null;
			boolean setProxyContext = false;
			Object target = null;
			TargetSource targetSource = this.advised.getTargetSource();
			try {
				// 如果EnableAspectJAutoProxy.exposeProxy=true,
				//则将当前代理对象保存到线程本地变量
				//那么可以在执行被代理对象(原对象)方法时通过AopContext.currentProxy获取代理对象调用方法
				// 实现方法内部也可以代理,否则直接调用方法切面不起效
				if (this.advised.exposeProxy) {
					// Make invocation available if necessary.
					oldProxy = AopContext.setCurrentProxy(proxy);
					setProxyContext = true;
				}
				// Get as late as possible to minimize the time we "own" the target, in case it comes from a pool...
				//被代理对象
				target = targetSource.getTarget();
				Class<?> targetClass = (target != null ? target.getClass() : null);
				// 获取拦截器,这里有两个注解@Transactional,@Async,这里会得到两个Advice,分别为AnnotationAsyncExecutionInterceptor(org.springframework.scheduling.annotation.AsyncAnnotationBeanPostProcessor.setBeanFactory(BeanFactory)),TransactionInterceptor(org.springframework.transaction.annotation.ProxyTransactionManagementConfiguration)
				List<Object> chain = this.advised.getInterceptorsAndDynamicInterceptionAdvice(method, targetClass);
				Object retVal;
				// Check whether we only have one InvokerInterceptor: that is,
				// no real advice, but just reflective invocation of the target.
				//没有InterceptionAdvice,直接调用方法
				if (chain.isEmpty() && Modifier.isPublic(method.getModifiers())) {
					// We can skip creating a MethodInvocation: just invoke the target directly.
					// Note that the final invoker must be an InvokerInterceptor, so we know
					// it does nothing but a reflective operation on the target, and no hot
					// swapping or fancy proxying.
					Object[] argsToUse = AopProxyUtils.adaptArgumentsIfNecessary(method, args);
					retVal = methodProxy.invoke(target, argsToUse);
				}
				else {
					// We need to create a method invocation...
					// 按InterceptionAdvice顺序调用链调用各个MethodInterceptor
					// 这里先AnnotationAsyncExecutionInterceptor开启异步执行
					// 然后TransactionInterceptor开启事务执行实际方法
					retVal = new CglibMethodInvocation(proxy, target, method, args, targetClass, chain, methodProxy).proceed();
				}
				//处理返回结果
				retVal = processReturnType(proxy, target, method, retVal);
				return retVal;
			}
			finally {
				if (target != null && !targetSource.isStatic()) {
					targetSource.releaseTarget(target);
				}
				if (setProxyContext) {
					// Restore old proxy.
					AopContext.setCurrentProxy(oldProxy);
				}
			}
		}

```
CglibMethodInvocation.proceed会递归调用所有的MethodInterceptor.invoke方法,传入自身,下标currentInterceptorIndex递增获取相应的MethodInterceptor

```java
//父类ReflectiveMethodInvocation.proceed方法
public Object proceed() throws Throwable {
		// We start with an index of -1 and increment early.
		// 所有拦截器执行完成,调用实际处理业务代码,实际切点方法
		if (this.currentInterceptorIndex == this.interceptorsAndDynamicMethodMatchers.size() - 1) {
			return invokeJoinpoint();
		}
		//获取拦截器
		Object interceptorOrInterceptionAdvice =
				this.interceptorsAndDynamicMethodMatchers.get(++this.currentInterceptorIndex);
		//如果是InterceptorAndDynamicMethodMatcher类型,判断是否满足条件,满足条件的调用拦截器的invoke方法,否则跳过拦截器,直接进入下一个递归
		if (interceptorOrInterceptionAdvice instanceof InterceptorAndDynamicMethodMatcher) {
			// Evaluate dynamic method matcher here: static part will already have
			// been evaluated and found to match.
			InterceptorAndDynamicMethodMatcher dm =
					(InterceptorAndDynamicMethodMatcher) interceptorOrInterceptionAdvice;
			Class<?> targetClass = (this.targetClass != null ? this.targetClass : this.method.getDeclaringClass());
			if (dm.methodMatcher.matches(this.method, targetClass, this.arguments)) {
				return dm.interceptor.invoke(this);
			}
			else {
				// Dynamic matching failed.
				// Skip this interceptor and invoke the next in the chain.
				// 跳过当前拦截器,执行下一个拦截器.
				return proceed();
			}
		}
		else {
			// It's an interceptor, so we just invoke it: The pointcut will have
			// been evaluated statically before this object was constructed.
			// 普通拦截器,直接调用invoke方法.
			return ((MethodInterceptor) interceptorOrInterceptionAdvice).invoke(this);
		}
	}
}

// ReflectiveMethodInvocation.invokeJoinpoint会调用AopUtils.invokeJoinpointUsingReflection(this.target, this.method, this.arguments);
// AopUtils.invokeJoinpointUsingReflection 直接通过反射调用实际方法,target是原生的bean,没有被代理过的对象.
public static Object invokeJoinpointUsingReflection(@Nullable Object target, Method method, Object[] args)throws Throwable {
	//...忽略Exception处理
	ReflectionUtils.makeAccessible(method);
	return method.invoke(target, args);
}

//子类CglibMethodInvocation重写了父类ReflectiveMethodInvocation.invokeJoinpoint方法,栗子中实际调用的是TransactionService.doInTransaction()方法
		protected Object invokeJoinpoint() throws Throwable {
			if (this.methodProxy != null) {
				return this.methodProxy.invoke(this.target, this.arguments);
			}
			else {
				return super.invokeJoinpoint();
			}
		}
```

####MethodProxy
调用中,有一个参数MethodProxy
生成的代理类TransactionService$$EnhancerBySpringCGLIB$$5c62f07e.class
```java
// forName3 为实际写的类,CGLIB$doInTransaction$0$Method指向的是代码写的方法doInTransaction
Class<?> forName = Class.forName("com.example.testtransactional.TransactionService$$EnhancerBySpringCGLIB$$5c62f07e");
final Class<?> forName3;
private static final Method CGLIB$doInTransaction$0$Method = ReflectUtils.findMethods(new String[] { "doInTransaction", "()V" }, (forName3 = Class.forName("com.example.testtransactional.TransactionService")).getDeclaredMethods())[0];
// 创建doInTransaction的methodProxy
private static final MethodProxy CGLIB$doInTransaction$0$Proxy= MethodProxy.create((Class)forName3, (Class)forName, "()V", "doInTransaction", "CGLIB$doInTransaction$0");
;
```

MethodProxy类定义:
```java
public class MethodProxy {
	//实际方法签名  doInTransaction()V
	private Signature sig1;
	//代理类方法签名  CGLIB$doInTransaction$0()V
	private Signature sig2;
	//实际类c1:TransactionService
	//与代理类c2:TransactionService$$EnhancerBySpringCGLIB$$5c62f07e
	private CreateInfo createInfo;

	private final Object initLock = new Object();

	private volatile FastClassInfo fastClassInfo;

	// 创建实际类c1:TransactionService与代理类c2:TransactionService$$EnhancerBySpringCGLIB$$5c62f07e关系
	public static MethodProxy create(Class c1, Class c2, String desc, String name1, String name2) {
		MethodProxy proxy = new MethodProxy();
		proxy.sig1 = new Signature(name1, desc);
		proxy.sig2 = new Signature(name2, desc);
		proxy.createInfo = new CreateInfo(c1, c2);
		return proxy;
	}

	// 创建fastClass
	// f1为实际类TransactionService的fastClass
	// f2为代理类TransactionService$$EnhancerBySpringCGLIB$$5c62f07e的fastClass
	// i1为fastClass1的对应代理方法索引
	// i2为fastClass2的对应代理方法索引
	// fastClass通过建立方法索引方式查询对应的方法直接调用,不需通过反射调用,查找通过switch case,查找一步到位.
	private void init() {
		if (fastClassInfo == null) {
			synchronized (initLock) {
				if (fastClassInfo == null) {
					CreateInfo ci = createInfo;

					FastClassInfo fci = new FastClassInfo();
					fci.f1 = helper(ci, ci.c1);
					fci.f2 = helper(ci, ci.c2);
					fci.i1 = fci.f1.getIndex(sig1);
					fci.i2 = fci.f2.getIndex(sig2);
					fastClassInfo = fci;
					createInfo = null;
				}
			}
		}
	}
	
	// 主要两个调用方法invoke,invokeSuper
	public Object invoke(Object obj, Object[] args) throws Throwable {
		try {
			init();
			FastClassInfo fci = fastClassInfo;
			// 调用f1的i1方法索引,栗子中就是TransactionService.doInTransaction()
			return fci.f1.invoke(fci.i1, obj, args);
		}
		catch (InvocationTargetException ex) {
			throw ex.getTargetException();
		}
		catch (IllegalArgumentException ex) {
			if (fastClassInfo.i1 < 0)
				throw new IllegalArgumentException("Protected method: " + sig1);
			throw ex;
		}
	}

	public Object invokeSuper(Object obj, Object[] args) throws Throwable {
		try {
			init();
			FastClassInfo fci = fastClassInfo;
			// 调用f2的i2方法索引,栗子中就是TransactionService$$EnhancerBySpringCGLIB$$5c62f07e.CGLIB$doInTransaction$0()
			return fci.f2.invoke(fci.i2, obj, args);
		}
		catch (InvocationTargetException e) {
			throw e.getTargetException();
		}
	}
}
```

