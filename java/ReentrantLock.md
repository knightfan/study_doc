##ReentrantLock
java.util.concurrent.locks.ReentrantLock是jdk1.5引入的可重入锁，类似于synchronized，比synchronized更灵活，可配置公平锁(先来先获取锁)，非公平锁(抢占式获取锁)
ReentrantLock类图
![ReentrantLock类图](pic/ReentrantLock.png)

###java.util.concurrent.locks.Lock
ReentrantLock实现了java.util.concurrent.locks.Lock接口，先看看Locks接口定义
```java
public interface Lock {
	//获取锁，如果获取不到(被其他线程占用)，则当前线程进入等待状态，直到锁可用为止
    void lock();
    //获取锁，如果获取不到(被其他线程占用)，则当前线程进入等待状态，直到锁可用为止或者被其他线程调用中断(Thread.interrupt),当前线程的中断标志会被清除掉，抛出InterruptedException。
    void lockInterruptibly() throws InterruptedException;
    //尝试获取锁，如果可用则锁定并立即返回true，不可用立即返回false
    boolean tryLock();
    //尝试获取锁，如果可用立即返回true，不可用等待超过参数传的时间返回false，等待期间如果锁可用则返回true，如果被中断(其他线程调用该线程的Thread.interrupt)，则抛出抛出InterruptedException
    boolean tryLock(long time, TimeUnit unit) throws InterruptedException;
    // 解锁
    void unlock();
    // 自定义等待条件
    Condition newCondition();
}
```

ReentrantLock 定义了3个内部类Sync(同步抽象类),FairSync(公平锁同步),NonfairSync(非公平锁同步类),提供了两个构造函数
```java
    // 无参数，默认是非公平的(抢占式，在获取锁的时候有可能成功，不进入等待队列)
    public ReentrantLock() {
        sync = new NonfairSync();
    }
    // 指定公平或非公平锁
	public ReentrantLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
    }
```
###java.util.concurrent.locks.ReentrantLock.Sync
Sync类继承了AbstractQueuedSynchronizer(AQS)
```java
  abstract static class Sync extends AbstractQueuedSynchronizer {
        /**
         * Performs {@link Lock#lock}. The main reason for subclassing
         * is to allow fast path for nonfair version.
         */
        abstract void lock();

        /**
         * Performs non-fair tryLock.  tryAcquire is implemented in
         * subclasses, but both need nonfair try for trylock method.
         * 非公平获取资源，acquires获取的数量，如果资源空闲，则通过CAS竞争获取，并设置当前线程为资源的持有者，如果当前线程是资源持有者，则直接往上加资源锁数量。
         */
        final boolean nonfairTryAcquire(int acquires) {
            final Thread current = Thread.currentThread();
            int c = getState();
            if (c == 0) {
            	//没人获得资源，通过CAS更新，更新成功则设置成当前线程。
                if (compareAndSetState(0, acquires)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }
            }
            // 如果不是0，则判断资源持有者是否是当前线程，如果是，往上加。		//释放
            else if (current == getExclusiveOwnerThread()) {
                int nextc = c + acquires;
                if (nextc < 0) // overflow
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            return false;
        }

		//释放,只有持有资源的线程才能释放，如果结果释放后结果为0，则表示可以由其他线程竞争，清空资源持有线程
        protected final boolean tryRelease(int releases) {
            int c = getState() - releases;
            if (Thread.currentThread() != getExclusiveOwnerThread())
                throw new IllegalMonitorStateException();
            boolean free = false;
            if (c == 0) {
                free = true;
                setExclusiveOwnerThread(null);
            }
            setState(c);
            return free;
        }

        final ConditionObject newCondition() {
            return new ConditionObject();
        }
    }
```
###java.util.concurrent.locks.ReentrantLock.FairSync
FairSync是ReentrantLock的内部类，Sync子类，如果发现等待队列有元素时直接进入等待队列。
```java
	 static final class FairSync extends Sync {
        private static final long serialVersionUID = -3000897897090466540L;
		//获取锁
        final void lock() {
            acquire(1);
        }

        /**
         * Fair version of tryAcquire.  Don't grant access unless
         * recursive call or no waiters or is first.
         */
        protected final boolean tryAcquire(int acquires) {
            final Thread current = Thread.currentThread();
            int c = getState();
            if (c == 0) {
            	//资源可用是，判断等待队列是否有前驱，没有的话并且通过CAS更新成功，则可获得锁。将资源的线程持有者更新成自己。
                if (!hasQueuedPredecessors() &&
                    compareAndSetState(0, acquires)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }
            }
            else if (current == getExclusiveOwnerThread()) {
                int nextc = c + acquires;
                if (nextc < 0)
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            return false;
        }
    }
```
###java.util.concurrent.locks.ReentrantLock.NonfairSync
NonfairSync在获取锁的时候，不会判断资源是否可用，队列是否为空，直接通过CAS更新state，更新成功则将资源的线程持有者更新成自己，否则进入等待队列。
```java
 static final class NonfairSync extends Sync {
        private static final long serialVersionUID = 7316153563782823691L;

        /**
         * Performs lock.  Try immediate barge, backing up to normal
         * acquire on failure.
         */
        final void lock() {
        	//获取锁时直接尝试通过CAS将state的0更新成1，更新成功则获取成功，否则跟公平锁一样获取失败进入等待队列。
            if (compareAndSetState(0, 1))
                setExclusiveOwnerThread(Thread.currentThread());
            else
                acquire(1);
        }

        protected final boolean tryAcquire(int acquires) {
            return nonfairTryAcquire(acquires);
        }
    }
```
###acquire(int)方法
FairSync,NonfairSync类中的lock方法都有调用到acquire(1)这个方法，acquire(int)位于他们的父类AbstractQueuedSynchronizer中，acquire(int)调用中
```java
    public final void acquire(int arg) {
    	//调用子类实现的tryAcquire方法，如果失败(返回false)则加入队列，节点模式为EXCLUSIVE排他。
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            selfInterrupt();
    }

	//addWaiter，添加到等待队列中，队列是一个双向链表
    //mode参数为两种模式，分别为共享与独占模式SHARED或EXCLUSIVE
	private Node addWaiter(Node mode) {
    	// 初始化队列节点，包括当前线程与节点模式
        Node node = new Node(Thread.currentThread(), mode);
        // 尝试CAS更新队列尾节点，如果失败则通过enq(Node)函数插入队列
        Node pred = tail;
        if (pred != null) {
            node.prev = pred;
            if (compareAndSetTail(pred, node)) {
                pred.next = node;
                return node;
            }
        }
        enq(node);
        return node;
    }
    //插入队列，通过CAS插入队列，不需通过系统同步机制来插入
    private Node enq(final Node node) {
        for (;;) {
            Node t = tail;
            if (t == null) { // 队列为空时，初始化一个空节点作为头节点，通过CAS来设置
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {
            	//通过CAS插入队列尾节点，将tail设置为node
                node.prev = t;
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    return t;
                }
            }
        }
    }
```


